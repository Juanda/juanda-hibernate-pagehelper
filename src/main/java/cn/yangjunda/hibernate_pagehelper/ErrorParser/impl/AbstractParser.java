package cn.yangjunda.hibernate_pagehelper.ErrorParser.impl;


import cn.yangjunda.hibernate_pagehelper.Dialect;
import cn.yangjunda.hibernate_pagehelper.ErrorParser.Parser;
import cn.yangjunda.hibernate_pagehelper.ErrorParser.SqlParser;

/**
 * Created by juanda on 12/6/17.
 */
public abstract class AbstractParser implements Parser {

    public static final SqlParser sqlParser = new SqlParser();

    public AbstractParser() {
    }

    public static Parser newParser(Dialect dialect) {
        Object parser = null;
        switch(dialect.ordinal()) {
            case 0:
                parser = new MysqlParser();
                break;
            case 1:
                parser = new SqlServer2012Dialect();
                break;
            case 2:
                parser = new OracleParser();
                break;
            case 3:
                parser = new SqlServerParser();
                break;
            default:
                throw new RuntimeException("分页插件" + dialect + "方言错误!");
        }

        return (Parser)parser;
    }

    public String getCountSql(String sql) {
        return sqlParser.getSimpleCountSql(sql);
    }

    public String getPageSql(String sql, String orderBy){
        return sqlParser.getPageSql(sql,orderBy);
    }
}
