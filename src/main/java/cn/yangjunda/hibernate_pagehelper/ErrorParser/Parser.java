package cn.yangjunda.hibernate_pagehelper.ErrorParser;

/**
 * Created by juanda on 12/6/17.
 */
public interface Parser {

    String getCountSql(String var1);

    String getPageSql(String sql, String orderBy);
}
