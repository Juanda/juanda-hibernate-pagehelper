package cn.yangjunda.hibernate_pagehelper;

/**
 * Created by juanda on 12/6/17.
 */
public class PageHelper {

    public PageHelper() {
    }

    public static long count(ISelect select) {
        Page page = startPage(1, -1, true);
        select.doSelect();
        return page.getTotal();
    }

    public static <E> Page<E> startPage(int pageNum, int pageSize) {
        return startPage(pageNum, pageSize, true);
    }

    public static <E> Page<E> startPage(int pageNum, int pageSize, boolean count) {
        return startPage(pageNum, pageSize, count, (Boolean)null);
    }

    public static <E> Page<E> startPage(int pageNum, int pageSize, String orderBy) {
        Page page = startPage(pageNum, pageSize);
        page.setOrderBy(orderBy);
        return page;
    }


    public static <E> Page<E> startPage(int pageNum, int pageSize, boolean count, Boolean reasonable) {
        return startPage(pageNum, pageSize, count, reasonable, (Boolean)null);
    }

    public static <E> Page<E> startPage(int pageNum, int pageSize, boolean count, Boolean reasonable, Boolean pageSizeZero) {
        if(pageNum<=0){
            pageNum = 1;
        }
        if(pageSize<=0){
            pageSize = 1;
        }
        Page page = new Page(pageNum, pageSize, count);
        page.setPageNum(pageNum).setPageSize(pageSize);
        page.setReasonable(reasonable);
        page.setPageSizeZero(pageSizeZero);
        return page;
    }


}
