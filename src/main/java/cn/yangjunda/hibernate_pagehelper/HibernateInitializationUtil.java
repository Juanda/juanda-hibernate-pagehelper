package cn.yangjunda.hibernate_pagehelper;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.dialect.*;
import org.hibernate.internal.SessionFactoryImpl;


/**
 * Created by Juanda on 2017/12/6.
 */
public class HibernateInitializationUtil {
    public static SessionFactory sessionFactory;
    static {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().configure( "hibernate.cfg.xml" )
                .build();
        Metadata metadata = new MetadataSources( standardRegistry )
                .getMetadataBuilder()
                .applyImplicitNamingStrategy(ImplicitNamingStrategyJpaCompliantImpl.INSTANCE ).build();
        sessionFactory = metadata.getSessionFactoryBuilder()
                .build();
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }

    public static org.hibernate.dialect.Dialect getDialect(){
        SessionFactoryImpl sessionFactory1 = (SessionFactoryImpl) sessionFactory;
        return sessionFactory1.getDialect();
    }

}
