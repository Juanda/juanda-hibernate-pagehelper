package cn.yangjunda.hibernate_pagehelper;

/**
 * Created by juanda on 12/6/17.
 */


public enum Dialect {
    mysql,
    sqlserver2012;

    private Dialect() {
    }

    public static Dialect of(String dialect) {
        try {
            Dialect e = valueOf(dialect.toLowerCase());
            return e;
        } catch (IllegalArgumentException var7) {
            String dialects = null;
            Dialect[] var3 = values();
            int var4 = var3.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                Dialect d = var3[var5];
                if(dialects == null) {
                    dialects = d.toString();
                } else {
                    dialects = dialects + "," + d;
                }
            }

            throw new IllegalArgumentException("Mybatis分页插件dialect参数值错误，可选值为[" + dialects + "]");
        }
    }

    public static String[] dialects() {
        Dialect[] dialects = values();
        String[] ds = new String[dialects.length];

        for(int i = 0; i < dialects.length; ++i) {
            ds[i] = dialects[i].toString();
        }

        return ds;
    }

    public static String fromJdbcUrl(String jdbcUrl) {
        String[] dialects = dialects();
        String[] var2 = dialects;
        int var3 = dialects.length;

        for(int var4 = 0; var4 < var3; ++var4) {
            String dialect = var2[var4];
            if(jdbcUrl.indexOf(":" + dialect + ":") != -1) {
                return dialect;
            }
        }

        return null;
    }


}

